# URL Shortener application

## How to run it

Just type  
`gradle bootRun`  
It will run an embedded web server (Tomcat by default).  
If you prefer, you can run
`gradle war`
and deploy manually the resulting file in the container of your choice.  

## Configuration

There is a file `application.properties` used for Redis connection. 

## Errors

Here the different error codes returned:  
*   URL\_NOT\_FOUND: If a given tiny URL does not match any stored value  
*   URL\_NOT\_VALID: If a given long URL is not URL  
*   SHORTENED\_URL\_TOO\_LONG: If a resulting hash is too long  
*   UNKNOWN\_ERROR: If an unexpected error occurred

## Alternatives/Choices

*   Use a provider. There is bunch of providers. Some of the most famous are [bit.ly]("bit.ly"), [http://tinyurl.com/]("tinyUrl") and [https://developers.google.com/url-shortener/]("Google URL Shortener").  
I assumed that implementation was required for showing my skills.
*   To store URLs mapping, we could use any database (SQL or not).  
Since it's a simple key/value with unique couples, Redis is well-known for his performances in such case.
*   Cloud database hosting. Reliability is an important criteria, AWS is well-knwon for his robustness.
 
## Possible enhancements

*   Integration testing using REST assured, by example
*   Secure API with an API key, JWT token, etc.