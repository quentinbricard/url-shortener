package com.notarius.controller;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.notarius.service.UrlShortenerService;

@SpringBootTest
public class UrlShortenerControllerTest {

   private static final String TINY_URL_PARAMETER = "tinyUrl";
   private static final String URL_SAMPLE = "http://example.com";
   private static final String URL_PARAMETER = "url";
   private static final String URLS_PATH = "/urls";
   private MockMvc mockMvc;
   private UrlShortenerService urlShortenerServiceMock;
   
   @Before
   public void setup() {
      urlShortenerServiceMock = mock(UrlShortenerService.class);
      UrlShortenerController urlShortenerController = new UrlShortenerController(urlShortenerServiceMock);
      mockMvc = MockMvcBuilders.standaloneSetup(urlShortenerController).build();
   }

   @Test
   public void testPost() throws Exception {
      mockMvc.perform(MockMvcRequestBuilders.post(URLS_PATH).param(URL_PARAMETER, URL_SAMPLE))
      .andExpect(MockMvcResultMatchers.status().isMethodNotAllowed());
   }
   
   @Test
   public void testPutNoParameter() throws Exception {
      mockMvc.perform(MockMvcRequestBuilders.put(URLS_PATH))
      .andExpect(MockMvcResultMatchers.status().isBadRequest());
   }
   
   @Test
   public void testPut() throws Exception {
      mockMvc.perform(MockMvcRequestBuilders.put(URLS_PATH).param(URL_PARAMETER, URL_SAMPLE))
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andExpect(MockMvcResultMatchers.content().string(notNullValue()));
   }
   
   @Test
   public void testGetNoParam() throws Exception {
      mockMvc.perform(MockMvcRequestBuilders.get(URLS_PATH))
      .andExpect(MockMvcResultMatchers.status().isBadRequest());
   }
   
   @Test
   public void testGet() throws Exception {
      mockMvc.perform(MockMvcRequestBuilders.get(URLS_PATH).param(TINY_URL_PARAMETER, URL_SAMPLE))
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andExpect(MockMvcResultMatchers.content().string(notNullValue()));
   }
   
}
