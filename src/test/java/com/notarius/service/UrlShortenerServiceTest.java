package com.notarius.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.util.ReflectionTestUtils;

import com.notarius.UrlShortenerExceptionMatcher;
import com.notarius.error.ErrorCode;
import com.notarius.error.UrlShortenerException;
import com.notarius.service.impl.UrlShortenerServiceImpl;
import com.notarius.validator.URLValidator;

public class UrlShortenerServiceTest {

   private static final int MAX_SIZE_URL = 10;
   private static final String ORIGINAL_URL = "originalURL";
   private static final String URL = "http://example.com";
   private UrlShortenerService urlShortenerService;
   
   private StringRedisTemplate stringRedisTemplateMock;
   private ValueOperations<String, String> valueOperationsMock;
   private URLValidator urlValidatorMock;
   @Rule
   public ExpectedException exception = ExpectedException.none();
   
   @SuppressWarnings("unchecked")
   @Before
   public void setUp() {
      stringRedisTemplateMock = mock(StringRedisTemplate.class);
      valueOperationsMock = mock(ValueOperations.class);
      when(stringRedisTemplateMock.opsForValue()).thenReturn(valueOperationsMock);
      urlValidatorMock = mock(URLValidator.class);
      urlShortenerService = new UrlShortenerServiceImpl(stringRedisTemplateMock, urlValidatorMock);  
      ReflectionTestUtils.setField(urlShortenerService, "maxSizeUrls", MAX_SIZE_URL);
      ReflectionTestUtils.setField(urlShortenerService, "domain", "http://example.com/");
   }
   
   @Test
   public void testNotValidURL() {
      String wrongUrl = "url";
      doThrow(new UrlShortenerException(ErrorCode.URL_NOT_VALID, null)).when(urlValidatorMock).validateURL(wrongUrl);
      exception.expect(UrlShortenerException.class);
      exception.expect(new UrlShortenerExceptionMatcher(new UrlShortenerException(ErrorCode.URL_NOT_VALID, null)));
      urlShortenerService.shortenURL(wrongUrl);
   }
   
   @Test
   public void testShortenTwiceURL() {
      String firstShortenedURL = urlShortenerService.shortenURL(URL);
      String secondShortenedURL = urlShortenerService.shortenURL(URL);
      assertEquals(firstShortenedURL, secondShortenedURL);
   }
   
   @Test
   public void testShortenedURLTooLong() {
      ReflectionTestUtils.setField(urlShortenerService, "maxSizeUrls", 0);
      exception.expect(UrlShortenerException.class);
      exception.expect(new UrlShortenerExceptionMatcher(new UrlShortenerException(ErrorCode.SHORTENED_URL_TOO_LONG, null)));
      urlShortenerService.shortenURL(URL);
   }
   @Test
   public void testLengthShortenedURL() {
      String shortenedURL = urlShortenerService.shortenURL(URL);
      assertNotNull(shortenedURL);
   }
   
   @Test
   public void testNotFoundURL() {
      exception.expect(UrlShortenerException.class);
      exception.expect(new UrlShortenerExceptionMatcher(new UrlShortenerException(ErrorCode.URL_NOT_FOUND, null)));
      urlShortenerService.retrieveOriginalURL("not_found");
   }
   
   @Test
   public void testSuccessURL() {
      when(valueOperationsMock.get(URL)).thenReturn(ORIGINAL_URL);
      String originalURL = urlShortenerService.retrieveOriginalURL(URL);
      assertEquals(ORIGINAL_URL, originalURL);
   }
}
