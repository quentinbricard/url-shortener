package com.notarius.validator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.validator.routines.UrlValidator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.notarius.UrlShortenerExceptionMatcher;
import com.notarius.error.ErrorCode;
import com.notarius.error.UrlShortenerException;
import com.notarius.validator.impl.URLValidatorImpl;

public class URLValidatorTest {

   private static final String URL = "url";
   private URLValidator urlValidator;
   private UrlValidator urlValidatorMock;
   
   @Rule
   public ExpectedException exception = ExpectedException.none();
   
   @Before
   public void setUp() {
      urlValidatorMock = mock(UrlValidator.class);
      urlValidator = new URLValidatorImpl(urlValidatorMock);
   }
   
   @Test
   public void testSuccess() {
      when(urlValidatorMock.isValid(URL)).thenReturn(true);
      urlValidator.validateURL(URL);
   }
   
   @Test
   public void testFailure() {
      exception.expect(UrlShortenerException.class);
      exception.expect(new UrlShortenerExceptionMatcher(new UrlShortenerException(ErrorCode.URL_NOT_VALID, null)));
      urlValidator.validateURL(URL);
   }
}
