package com.notarius;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import com.google.common.base.Objects;
import com.notarius.error.UrlShortenerException;

public class UrlShortenerExceptionMatcher extends TypeSafeMatcher<UrlShortenerException> {

   private final UrlShortenerException urlShortenerException;
   
   public UrlShortenerExceptionMatcher(final UrlShortenerException urlShortenerException) {
       this.urlShortenerException = urlShortenerException;
   }
   
   @Override
   public void describeTo(Description description) {
      description.appendText("matches exception ")
      .appendValue(urlShortenerException.getMessage());
   }

   @Override
   protected boolean matchesSafely(UrlShortenerException item) {
      return Objects.equal(urlShortenerException.getErrorCode().getCode(), item.getErrorCode().getCode());
   }
   
}