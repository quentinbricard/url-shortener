package com.notarius.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.notarius.service.UrlShortenerService;

@RestController
@RequestMapping("/urls")
public class UrlShortenerController {
   private static final Logger LOGGER = LoggerFactory.getLogger(UrlShortenerController.class);

   public final UrlShortenerService urlShortenerService;

   @Autowired
   public UrlShortenerController(final UrlShortenerService urlShortenerService) {
      this.urlShortenerService = urlShortenerService;
   }

   @RequestMapping(method = RequestMethod.PUT)
   public ResponseEntity<String> shortenURL(@RequestParam("url") String url) {
      LOGGER.debug("Shortening URL {}...", url);
      String shortenedURL = urlShortenerService.shortenURL(url);
      LOGGER.debug("Shortened URL result is {}", shortenedURL);
      return new ResponseEntity<>(shortenedURL, HttpStatus.OK);
   }
   
   @RequestMapping(method = RequestMethod.GET)
   public ResponseEntity<String> retrieveOriginalURL(@RequestParam("tinyUrl") String tinyUrl) {
      LOGGER.debug("Retrieve original URL from {}...", tinyUrl);
      String originalURL = urlShortenerService.retrieveOriginalURL(tinyUrl);
      LOGGER.debug("Retrieved original URL {}", originalURL);
      return new ResponseEntity<>(originalURL, HttpStatus.OK);
   }
}
