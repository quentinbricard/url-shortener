package com.notarius.validator;

import com.notarius.error.UrlShortenerException;

/**
 * <p>Validator used to ensure a given url is valid.</p>
 * @author quentinbricard
 *
 */
@FunctionalInterface
public interface URLValidator {

   /**
    * <p>Ensures that the given URL is valid.</p>
    * <p>If it is not valid, a {@link UrlShortenerException} is thrown.</p>
    * @param url the given url
    * @throws UrlShortenerException if url is not valid
    */
   void validateURL(String url);
}
