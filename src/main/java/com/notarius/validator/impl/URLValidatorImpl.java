/**
 * 
 */
package com.notarius.validator.impl;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;

import com.notarius.error.ErrorCode;
import com.notarius.error.UrlShortenerException;
import com.notarius.validator.URLValidator;

/**
 * <p>Implementation of {@link URLValidator}.</p>
 * @author quentinbricard
 *
 */
@Component
public class URLValidatorImpl implements URLValidator {

   private final UrlValidator urlValidator;
   
   public URLValidatorImpl(final UrlValidator urlValidator) {
      this.urlValidator = urlValidator;
   }
   
   /* (non-Javadoc)
    * @see com.notarius.validator.URLValidator#validateURL(java.lang.String)
    */
   @Override
   public void validateURL(String url) {
      if(!urlValidator.isValid(url)) {
         throw new UrlShortenerException(ErrorCode.URL_NOT_VALID, String.format("Given url %s is not valid", url));
      }
   }

}
