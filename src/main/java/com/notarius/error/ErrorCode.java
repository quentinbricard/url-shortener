package com.notarius.error;

/**
 * <p>Used to describe the different errors that could occur.</p>
 * @author quentinbricard
 *
 */
public enum ErrorCode {
   URL_NOT_FOUND("URL_NOT_FOUND"), 
   URL_NOT_VALID("URL_NOT_VALID"),
   SHORTENED_URL_TOO_LONG("SHORTENED_URL_TOO_LONG"),
   UNKNOWN_ERROR("UNKNOWN_ERROR");

   private final String code;

   private ErrorCode(final String code) {
      this.code = code;
   }

   public String getCode() {
      return code;
   }
}
