package com.notarius.error;

/**
 * <p>Exception thrown when an operation fails on an operation regarding URL.</p>
 * <p>Mainly used to wrap an {@link ErrorCode}.</p>
 * 
 * @author quentinbricard
 *
 */
public class UrlShortenerException extends RuntimeException {

   private static final long serialVersionUID = -6691867903419470608L;

   private final ErrorCode errorCode;

   public UrlShortenerException(final ErrorCode errorCode, final String message, final Throwable cause) {
      super(message, cause);
      this.errorCode = errorCode;
   }

   public UrlShortenerException(final ErrorCode errorCode, final String message) {
      this(errorCode, message, null);
   }

   public ErrorCode getErrorCode() {
      return errorCode;
   }
}
