package com.notarius.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>Global handler to handle any {@link UrlShortenerException} occurring.</p>
 * @author quentinbricard
 *
 */
@ControllerAdvice
public class ErrorHandler {

   /**
    * <p>Used to catch a {@link UrlShortenerException} and build an associated {@link ResponseEntity}.</p>
    * @param exception the {@link UrlShortenerException} that occurred
    * @return the built {@link ResponseEntity}
    */
   @ExceptionHandler(UrlShortenerException.class)
   @ResponseBody
   public ResponseEntity<UrlShortenerException> handleException(UrlShortenerException exception) {
      return new ResponseEntity<>(exception, HttpStatus.BAD_REQUEST);
   }
}
