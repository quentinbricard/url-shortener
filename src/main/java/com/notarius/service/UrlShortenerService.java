package com.notarius.service;

import com.notarius.error.UrlShortenerException;

/**
 * <p>Service in charge of translating URL into tiny format and do the opposite.</p>
 * @author quentinbricard
 *
 */
public interface UrlShortenerService {
   
   /**
    * <p>Generate a unique tiny URL based on the given URL.<p>
    * @param url the given url
    * @return a <strong>unique</strong> tiny URL as a {@link String}
    */
   String shortenURL(String url);

   /**
    * <p>Retrieves a unique tiny URL based on the tiny formatted URL.<p>
    * <p>If no matching url is foud, a {@link UrlShortenerException} is thrown</p>
    * @param url the shortened url
    * @return the retrieved original URL as a {@link String}
    * @throws UrlShortenerException if no matching URL is found
    */
   String retrieveOriginalURL(String url);
}
