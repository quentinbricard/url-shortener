/**
 * 
 */
package com.notarius.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.hash.Hashing;
import com.notarius.error.ErrorCode;
import com.notarius.error.UrlShortenerException;
import com.notarius.service.UrlShortenerService;
import com.notarius.validator.URLValidator;

/**
 * <p>
 * Implementation of {@link UrlShortenerService}.
 * </p>
 * <p>
 * Used with a repository to save and retrieve URLs.
 * </p>
 * 
 * @author quentinbricard
 *
 */
@Service
@PropertySource(value="urlShortener.properties")
public class UrlShortenerServiceImpl implements UrlShortenerService {

   private static final String NO_MATCHING_URL_ERROR = "The given url %s has no matching url";
   
   @Value("${url_shortener.domain}")
   private String domain;
   @Value("${url_shortener.max_size_urls}")
   private int maxSizeUrls;
   
   private final StringRedisTemplate stringRedisTemplate;
   private final URLValidator urlValidator;
   
   @Autowired
   public UrlShortenerServiceImpl(final StringRedisTemplate stringRedisTemplate, final URLValidator urlValidator) {
      this.stringRedisTemplate = stringRedisTemplate;
      this.urlValidator = urlValidator;
   }

   /*
    * (non-Javadoc)
    * 
    * @see com.notarius.service.UrlShortenerService#shortenURL(java.lang.String)
    */
   @Transactional
   @Override
   public String shortenURL(String url) {
      urlValidator.validateURL(url);
      String hash = Hashing.murmur3_32().hashUnencodedChars(url).toString();
      if(hash.length() > maxSizeUrls) {
         throw new UrlShortenerException(ErrorCode.SHORTENED_URL_TOO_LONG, String.format("Resulting hash %s is too long. Max size expected %d vs actual %d", hash, maxSizeUrls, hash.length()));
      }
      String shortenedURL = domain + hash;
      stringRedisTemplate.opsForValue().set(shortenedURL, url);
      return shortenedURL;
   }

   /*
    * (non-Javadoc)
    * 
    * @see
    * com.notarius.service.UrlShortenerService#retrieveOriginalURL(java.lang.
    * String)
    */
   @Override
   public String retrieveOriginalURL(String url) {
      String originalUrl = stringRedisTemplate.opsForValue().get(url);
      if(originalUrl == null) {
         throw new UrlShortenerException(ErrorCode.URL_NOT_FOUND, String.format(NO_MATCHING_URL_ERROR, url));
      }
      return originalUrl;
   }

}
